/**
 * To remove elements
 * 
 * @param {String} selector - identifying the elements to be removed
 */
function removeElements(selector) {
    $(selector).remove();
}

/**
 * convert date to specified timezone
 * @param {Date} date 
 * @param {Object} options 
 */
function getDateObject(date, options) {
    return new Date(date).toLocaleString('en-AU', { hour12: false, timeZone: options.timeZone });
}

/**
 * get date string
 * @param {Date} date 
 * @param {Object} options 
 */
function getDate(date, options) {
    return getDateObject(date, options).split(',')[0];
}

/**
 * get time string
 * @param {Date} date 
 * @param {Object} options 
 */
function getTime(date, options) {
    return getDateObject(date, options).split(',')[1].substring(1, 6);
}

/**
 * check if field is null / undefined / empty
 * 
 * @param {any} field 
 */
function checkIfNull(field) {
    if (field) {
        return false;
    }
    return true;
}

/**
 * Validate fields and inserts a valdiation message if field is null
 * @param fields - array of required form fields
 * 
 * uses data-name attribute of element to get field name
 */
function validateReqdFields(fields) {
    var validated = true;

    $.each(fields, function (index, element) {
        if (checkIfNull($(element).val())) {
            validated = false;
            $(element).siblings('.validate-reqd-field').length ?
                null :
                $(element).after(`<div class="text-danger validate-reqd-field">Please fill the ${$(element).data('name')} field</div>`);
        }
    });

    return validated;
}

function padNumber(number, digits) {
    var i = digits - number.toString().length;

    while (i > 0) {
        number = '0' + number;
        i--;
    }

    return number;
}

/**
 * convert mins into HH:MM format
 * @param {*} mins 
 */
function calcDuration(mins) {
    return padNumber(Math.floor(mins / 60), 2) + ':' + padNumber(mins % 60, 2);
}

/**
 * Splits camel cased strings according to the options provided
 * @param {String} str 
 * @param {Object} options Object having separator, toLower, toUpper attributes
 */
function splitCamelCase(str, options) {

    //if string is empty/null/undefined
    if (!str) {
        return 'Please provide a valid string';
    };

    options = options || {};

    var splitStr = '',
        regExp = /[A-Z]/g,
        prevIndex = 0,
        execObj;

    while (execObj = regExp.exec(str)) {
        splitStr ?
            (splitStr += (options.separator || '') + str.slice(prevIndex, execObj.index)) :
            (splitStr = str.slice(prevIndex, execObj.index));

        prevIndex = execObj.index;
    }

    splitStr += (options.separator || '') + str.substring(prevIndex);

    return options.toLower && options.toUpper ? 'Error: String cannot have both upper and lower cases' : options.toLower ? splitStr.toLowerCase() : options.toUpper ? splitStr.toUpperCase() : splitStr;
};

/**
 * HTML template to get search filter form based on the options
 * @param {Object} options - having type of form as attribute
 */
function getSearchFilterTpl(options) {

    //split camel case words using '-' separator
    var prefixId = splitCamelCase(options.type, { separator: '-', toLower: true }),
        name = options.type;

    return `<div class="tile">
                <form name=${name} novalidate>
                    <div class="form-group required">
                        <label for="${prefixId}-from" class="control-label">
                            From
                        </label>
                        <input type="text" id="${prefixId}-from" class="form-control" name="${prefixId}-from" placeholder="From" data-name="From" required>
                    </div>
                    <div class="form-group required">
                        <label for="${prefixId}-to" class="control-label">
                            To
                        </label>
                        <input type="text" id="${prefixId}-to" class="form-control" name="${prefixId}-to" placeholder="To" data-name="To" required>
                    </div>
                    <div class="form-group required">
                        <label for="${prefixId}-date" class="control-label">
                            Date
                        </label>
                        <input type="text" id="${prefixId}-date" name="${prefixId}-date" placeholder="Date" class="form-control" 
                            onfocus="(this.type='date')" onblur="(this.type='text')" data-name="Date" required>
                    </div>
                </form>
            </div>`;
};

/**
 * get template for flight details table
 * @param {Object} options 
 */
function getFlightDetailsTpl(options) {
    var tpl = `<div class="flight-details">
        <table class="table table-hover tile">
            <thead>
                <tr>
                    <th>
                        Flight Details
                    </th>
                    <th>
                        Departs
                    </th>
                    <th>
                        Time
                    </th>
                    <th>
                        Arrivals
                    </th>
                    <th>
                        Fare
                    </th>
                </tr>
            </thead>
            <tbody>`;

    options.rows.forEach(function (row) {
        tpl += `<tr>
                    <td>
                        <div class="txt-bg">${row.airline.code}${row.flightNum}</div>
                        <div>${row.airline.name}</div>
                    </td>
                    <td>
                        <div class="txt-bg">${getTime(row.start.dateTime, { timeZone: row.start.timeZone })}</div>
                        <div>${getDate(row.start.dateTime, { timeZone: row.start.timeZone })}</div>
                        <div>${row.start.airportName}</div>
                        <div>${row.start.countryName}</div>
                    </td>
                    <td>
                        <div class="txt-bg"><i class="fa fa-arrow-right"></i></div>
                        <div>${calcDuration(row.durationMin)}</div>
                    </td>
                    <td>
                        <div class="txt-bg">${getTime(row.finish.dateTime, { timeZone: row.finish.timeZone })}</div>
                        <div>${getDate(row.finish.dateTime, { timeZone: row.finish.timeZone })}</div>
                        <div>${row.finish.airportName}</div>
                        <div>${row.finish.countryName}</div>
                    </td>
                    <td>
                        <div class="txt-bg">AU$ ${row.price}</i></div>
                    </td>
                </tr>`
    });

    tpl += `</tbody>
        </table>
    </div>`;

    return tpl;
}

/**
 * make GET request and return data
 * @param {String} url 
 */
function makeGetRequest(url) {
    function promise(resolve, reject) {
        $.ajax({
            url: url,
            type: 'GET',
            success: function (data) {
                resolve(data);
            },
            error: function (error) {
                reject(error);
            }
        })
    }

    return new Promise(promise);
}

//extend jQuery to attach plugins
(function ($) {

    /**
     * attaches a filter form to DOM depending on the type of form
     * @param {Object} options
     */
    $.fn.attachFilterForm = function (options) {
        options = $.extend({}, $.fn.attachFilterForm.defaults, options);

        $(this).after(getSearchFilterTpl(options));
    };

    $.fn.attachFilterForm.defaults = {
        type: 'oneWay'
    };

    /**
     * get search results from the form; query params would be appended to the url provided
     * @param {String} url
     */
    $.fn.getSearchResults = function (url) {
        var form = $(this);

        var params = {
            from: $(form).find('input[id$=-from]').val(),
            to: $(form).find('input[id$=-to]').val(),
            date: $(form).find('input[id$=-date]').val()
        };

        return makeGetRequest(`${url}?date=${params.date}&from=${params.from}&to=${params.to}`);
    }

    /**
     * build table for flight details
     * @param {Array} data
     */
    $.fn.buildFlightDetails = function (data) {
        var element = this,
            html = '';

        data.forEach(function (tableData) {
            html += getFlightDetailsTpl({
                rows: tableData.details
            });
        });

        $(element).html(html);

        (data.length === 2) ?
            ($(element).find('.flight-details').addClass('split')) :
            ($(element).find('.flight-details').addClass('single'));
    }

})(jQuery);;