//fire on page load
$(function () {
    //demonstrating appending filter form using custom plugin & default options
    $('#one-way-heading').attachFilterForm();

    $('#if-return-trip-chk').on('change', function () {
        if ($(this).is(':checked')) {
            //demonstrating appending filter form using custom plugin & provided options
            $('#two-way-heading').attachFilterForm({
                type: 'returnTrip'
            });
        }
        else {
            $('#two-way-heading').siblings('.tile').remove();
        }
    });

    $('#btn-search').on('click', function () {
        removeElements('.search-panel .text-danger');

        if (validateForm()) {
            blockUI();

            var promises = [$('form[name=oneWay]').getSearchResults('/search')];

            $('#if-return-trip-chk').is(':checked') ?
                promises.push($('form[name=returnTrip]').getSearchResults('/search')) :
                null;

            Promise.all(promises)
                .then(function (response) {
                    $('#search-results').buildFlightDetails(response);

                    unblockUI();
                })
                .catch(function (error) {
                    unblockUI();

                    try {
                        alert('Error fetching flight details: ' + JSON.parse(error.responseText).error);
                    }
                    catch (err) {
                        alert('Error fetching flight details: ' + err.message);
                    }
                });
        }
    });

    $('#btn-clear').on('click', function () {
        $('.search-panel input').val('');
        $('.search-panel .text-danger, .flight-details').remove();
    });

});

/**
 * to block UI
 */
function blockUI() {
    $('.backdrop').show();
}

/**
 * to unblock UI
 */
function unblockUI() {
    $('.backdrop').hide();
}

/**
 * Validate form on click of search
 */
function validateForm() {
    return validateReqdFields($('*[required]'));
}