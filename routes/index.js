var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', {
    title: 'Travelport Locomote',
    boldTitle: 'Travelport',
    normalTitle: 'Locomote'
  });
});

module.exports = router;
