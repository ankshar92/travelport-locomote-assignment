"use strict";

var http = require('http');
var flightAPI = {};

/**
 * object containing api endpoint details
 */
const option = {
  host: 'node.locomote.com',
  port: '80',
  headers: {
    'Content-Type': 'application/json; charset=utf-8'
  }
};

/**
 * 
 * @param {Object} request - object accepting the path for the API call
 */
function makeAPICall(request) {
  return new Promise(function (resolve, reject) {
    try {

      //merging option & request objects into options
      let options = Object.assign({}, option, request);

      let msg = '';

      const req = http.request(options, function (res) {
        res.on('data', function (chunk) {
          msg += chunk;
        });

        res.on('end', function () {
          if (res.statusCode === 200) {
            resolve(flightAPI.sendResposne(msg));
          } else {
            reject({
              statusCode: res.statusCode,
              message: msg
            });
          }
        })
      });

      req.on('error', function (err) {
        reject(err);
      })

      req.end();
    }
    catch (err) {
      reject(err);
    };
  });
}

flightAPI = {
  sendResposne: function (data) {
    return {
      status: 'Y',
      details: data
    }
  },
  sendErrorResponse: function (error) {
    return {
      status: 'N',
      error: error
    }
  },
  getAirlines: function () {
    return makeAPICall({
      path: '/code-task/airlines'
    });
  },
  getAirports: function (query) {
    return makeAPICall({
      path: `/code-task/airports?q=${query}`
    });
  },
  searchFlights: function (queryParams) {
    let airlines = [],
      promises = [],
      flights = [],
      api = this;

    return new Promise(function (resolve, reject) {

      //retrieve all airlines details having flight code
      api.getAirlines()
        .then(function (data) {
          airlines = data.details;
          try {
            airlines = JSON.parse(airlines);
          }
          catch (err) {
            reject({
              message: 'Unable to parse airlines data.'
            });
          }

          //make array containing promises for single airline to search for flights with query params provided
          airlines.forEach(function (element) {
            promises.push(makeAPICall({
              path: `/code-task/flight_search/${element.code}?date=${queryParams.date}&from=${queryParams.from}&to=${queryParams.to}`
            }));
          });

          //resolve when all the end points return the data
          Promise.all(promises)
            .then(function (data) {
              try {
                //remove new line character from response
                data = JSON.parse(JSON.stringify(data).replace(/\\n/g, ''));

                //combine flight details into single array
                data.forEach(function (element) {
                  flights = flights.concat(JSON.parse(element.details));
                });

                resolve(api.sendResposne(flights));
              }
              catch (err) {
                reject(err);
              };
            })
            .catch(function (err) {
              reject(err);
            });

        })
        .catch(function (err) {
          reject(err);
        });
    });
  }
};

module.exports = flightAPI;