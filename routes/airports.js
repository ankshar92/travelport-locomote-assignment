var express = require('express');
var router = express.Router();
var flightAPI = require('./flightAPI');

router.get('/:query?', function (req, res, next) {
  flightAPI.getAirports(req.params.query || '')
    .then(function (data) {
      res.json(data);
    })
    .catch(function (error) {
      res.status(error.statusCode || 500).json(flightAPI.sendErrorResponse(error.message));
    });
});

module.exports = router;