"use strict";

var express = require('express');
var router = express.Router();
var flightAPI = require('./flightAPI');

router.get('', function (req, res) {
  flightAPI.searchFlights(req.query)
    .then(function (data) {
      res.json(data);
    })
    .catch(function (error) {
      res.status(error.statusCode || 500).json(flightAPI.sendErrorResponse(error.message));
    });
});

module.exports = router;
