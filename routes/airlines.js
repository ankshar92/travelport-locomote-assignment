"use strict";

var express = require('express');
var routes = express.Router();
var flightAPI = require('./flightAPI');

routes.get('/', function (req, res, next) {
  flightAPI.getAirlines()
    .then(function (data) {
      res.json(data);
    })
    .catch(function (error) {
      res.status(error.statusCode || 500).json(flightAPI.sendErrorResponse(error.message));
    });
});

module.exports = routes;